import mill._, scalalib._

object jar2nix extends ScalaModule {
  def scalaVersion = "2.13.1"
  def ivyDeps = Agg(
    ivy"io.get-coursier::coursier:2.0.0-RC2-4",
    ivy"io.get-coursier::coursier-cache:2.0.0-RC2-4",
    ivy"com.github.scopt::scopt:4.0.0-RC2"
  )
}
