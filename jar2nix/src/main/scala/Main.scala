package jar2nix

import coursier._
import coursier.cache.Cache
import java.io._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.Source
import scopt.OParser

case class NixSource(url: String, sha1: String) {
  def toNixFetchurl = s"""fetchurl {
      |  url = "${url}";
      |  sha1 = "${sha1}";
      |}""".stripMargin
}

case class NixDerivation(name: String, version: String, sources: Seq[NixSource], mainClass: String, javaOpts: String, packages: Seq[String]) {
  def toNix = {
    val template = Source.fromResource("default.nix").mkString
    val nixSources = sources.map(_.toNixFetchurl).map("(" + _ + ")").mkString(" ")
    template
      .replaceAll("%%name%%", name)
      .replaceAll("%%version%%", version)
      .replaceAll("%%sources%%", nixSources)
      .replaceAll("%%mainclass%%", mainClass)
      .replaceAll("%%javaopts%%", javaOpts)
      .replaceAll("%%packagesInPath%%", packages.mkString(" "))
  }
}

object Main extends App {
  case class Config(
    jar: String = "",
    name: String = "jar2nix",
    version: String = "0.0.0",
    mainClass: String = "",
    javaOpts: String = "",
    packages: Seq[String] = Nil,
  )

  val cliConfigParser = {
    val builder = OParser.builder[Config]
    import builder._
    OParser.sequence(
      programName("jar2nix"),
      help("help").text("print this usage text"),
      opt[String]('j', "jar")
        .action((v, c) => c.copy(jar = v))
        .text("jar to load"),
      opt[String]('m', "mainclass")
        .action((v, c) => c.copy(mainClass = v))
        .text("main class to load"),
      opt[String]('v', "version")
        .action((v, c) => c.copy(version = v))
        .text("version"),
      opt[String]('n', "name")
        .action((v, c) => c.copy(name = v))
        .text("name of the derivation"),
      opt[String]('o', "javaopts")
        .action((v, c) => c.copy(javaOpts = v))
        .text("options to pass to java"),
      opt[String]('p', "packages")
        .action((v, c) => c.copy(packages = v.split(",")))
        .text("packages to add in path (comma separated list of names)"),
    )
  }

  OParser.parse(cliConfigParser, args, Config()) match {
    case None => System.exit(0)
    case Some(config) =>
      val parts = config.jar.split(':')
      val organization = Organization(parts( 0 ))
      val moduleName = ModuleName(parts( 1 ))
      val project = Dependency(new Module(organization, moduleName, Map.empty), config.version)

      val resolution = Resolve().addDependencies(project).run()

      val sources = resolution.artifacts.map { artifact =>
        val sha1File = artifact.checksumUrls("SHA-1")
        val hash = Source.fromURL(sha1File).mkString
        NixSource(artifact.url, hash)
      }

      val derivation = NixDerivation(config.name, config.version, sources, config.mainClass, config.javaOpts, config.packages)

      val writer = new BufferedWriter(new FileWriter(new File("default.nix")))
      writer.write(derivation.toNix)
      writer.close()
  }
}

