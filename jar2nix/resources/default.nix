{ pkgs, stdenv, lib, fetchurl, jre }: let

srcs = [
%%sources%%
];

classPath = pkgs.lib.concatStringsSep ":" srcs;
packagesInPath = with pkgs; [ %%packagesInPath%% ];
extraPath = pkgs.lib.makeBinPath packagesInPath;
exportPath = pkgs.lib.optionalString (builtins.length packagesInPath != 0) "export PATH=${extraPath}:$PATH";

in pkgs.writeShellScriptBin "%%name%%" ''
  ${exportPath}

  classPath=${classPath}

  exec ${jre}/bin/java -cp "$classPath" %%javaopts%% %%mainclass%%
''
