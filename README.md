jar2nix
=======

Create a Nix derivation for a jar file by resolving and listing all its dependencies.

Usage
-----

To get all the available options use:

    mill jar2nix.run --help

This will generate a file `default.nix` that you can use in your system configuration:

    environment.systemPackages = [ (pkgs.callPackage ./default.nix {}) ];

Examples
--------

Package bloop:

    mill jar2nix.run -j ch.epfl.scala:bloop-frontend_2.12 -v 1.3.2 -n bloop-server -m bloop.Server

Package metals:

    mill jar2nix.run -j org.scalameta:metals_2.12 -v 0.7.0 -m scala.meta.metals.Main \
        -n metals -p python -o '-Xss4m -Xms100m -Dmetals.client=coc.nvim'

